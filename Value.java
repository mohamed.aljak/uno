public enum Value
{
    ZERO("0  "),
    ONE("1  "),
    TWO("2  "),
    THREE("3  "),
    FOUR("4  "),
    FIVE("5  "),
    SIX("6  "),
    SEVEN("7  "),
    EIGHT("8  "),
    NINE("9  "),
    DRAW_TWO("+2 "),
    REVERSE("<--"),
    SKIP("-->"),
    WILD("W  "),
    WILD_DRAW_FOUR("+4 ");

    private String valueShortForm;

    Value(String valueShortForm)
    {
        this.valueShortForm = valueShortForm;
    }

    public String getValueShortForm()
    {
        return this.valueShortForm;
    }
    public String toString()
    {
        switch (this)
        {
            case ZERO :          return "  ZERO  ";
            case ONE  :          return "  ONE   ";
            case TWO  :          return "  TWO   ";
            case THREE:          return "  THREE ";
            case FOUR :          return "  FOUR  ";
            case FIVE :          return "  FIVE  ";
            case SIX  :          return "  SIX   ";
            case SEVEN:          return "  SEVEN ";
            case EIGHT:          return "  EIGHT ";
            case NINE :          return "  NINE  ";
            case DRAW_TWO:       return "DRAW TWO";
            case REVERSE:        return " REVERSE";
            case SKIP:           return " !SKIP! ";
            case WILD:           return " !WILD! ";
            case WILD_DRAW_FOUR: return "!DRAW 4!";
            default   :          return "";
        }
    }
}
