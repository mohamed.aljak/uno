public class Card
{
    private Color color;
    private Value value;

    public Card() {}
    public Card(Color color,Value value)
    {
        this.color = color;
        this.value = value;
    }
    public String toString()
    {
        return this.color + "" + this.value;
    }
    public String[] asArray()
    {
        String[] toReturn = new String[7];
        toReturn[0]= " ___________ ";
        toReturn[1]= "|" + this.value.getValueShortForm() + "        |";
        toReturn[2]= "|   ^^^^^   |";
        toReturn[3]= "|  "  +this + " |";
        toReturn[4]= "|   ^^^^^   |";
        toReturn[5]= "|        " + this.value.getValueShortForm() + "|";
        toReturn[6]= "|___________|";
        return toReturn;
    }
    public String printCard()
    {
        String builder = "";
        for(int i = 0; i < 7; i++)
        {
            builder += "                                ";
            builder += this.getColor() + this.asArray()[i];
            builder += "\n";
        }
        builder += Color.RESET;
        return builder;
    }


    public Color getColor()
    {
        return this.color;
    }

    public Value getValue()
    {
         return this.value;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public void setValue(Value value)
    {
        this.value = value;
    }


}
