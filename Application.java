import java.util.*;

public class Application
{
    public static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args)
    {
        //Pre-Game start initialization and method calls
        CardPile drawPile = new CardPile();
        drawPile.generateDrawPile();
        CardPile discardPile = new CardPile();
        discardPile.drawCards(drawPile, 1);
        int currentPlayer = 0;

        //Creating players and their respective hands
        int playersAmount = getPlayersAmount();
        CardPile[] hands = generateHands(drawPile, playersAmount);

        //Begin and end game sequences
        String[] winningPlayers = startGame(drawPile, discardPile, hands, currentPlayer);
        printWinners(winningPlayers);
    }
    public static int getPlayersAmount()
    {
        int playersAmount;
        while(true)
        {
            System.out.println("How many Players? [2-10]");
            try
            {
                playersAmount = Integer.parseInt(sanitizeInput(scanner.nextLine()));
                if(playersAmount <2 || playersAmount > 10)
                {
                    throw new IllegalArgumentException("");
                }
                break;
            } catch (Exception e)
            {
                System.out.println("INCORRECT INPUT: RE-ENTER");
            }
        }
        return playersAmount;
    }
    public static CardPile[] generateHands(CardPile drawPile, int playersAmount)
    {
        CardPile[] hands = new CardPile[playersAmount];
        for(int i = 0; i< playersAmount; i++)
        {
            hands[i] = new CardPile();
            hands[i].drawCards(drawPile, 1);
        }
        return hands;
    }
    public static String[] startGame(CardPile drawPile,CardPile discardPile, CardPile[] hands, int currentPlayer)
    {
        //Initializing values:
        String[] winningPlayers = new String[hands.length];
        String[] playerNames = getPlayerNames(hands.length);
        int winnersAmount = 0;
        String rotation = "NORMAL";

        //Game Loop:
        while(true)
        {
            //Information displayed at each turn
            flush();
            System.out.println(discardPile.getTopCard().printCard());
            System.out.println(hands[currentPlayer]);
            System.out.println(playerNames[currentPlayer] + "'s turn:");
            System.out.println("Play a card, -1 to skip/draw card");

            //Processing user input
            int playInput = userInput(hands[currentPlayer],discardPile);
            if(playInput == -1)
            {
                hands[currentPlayer].drawCards(drawPile, 1);
                currentPlayer = nextPlayer(rotation, currentPlayer, hands);
                continue;
            }
            else
            {
                hands[currentPlayer].place(discardPile, playInput-1);
            }

            //Check if the current player has won. Remove from current playing hands and add to winningPlayers array.

            if (hands[currentPlayer].isEmpty())
            {
                System.out.println("empty deck");
                winningPlayers[winnersAmount] = playerNames[currentPlayer];
                for(int i = currentPlayer; i < hands.length -1; i++)
                {
                    hands[i] = hands[i+1];
                    playerNames[i] = playerNames[i+1];
                }
                CardPile[] newHands = new CardPile[hands.length -1];
                for(int i = 0; i < newHands.length; i++)
                {
                    newHands[i] = hands[i];
                }
                hands = newHands;
                winnersAmount ++;
                if(hands.length == 1)
                {
                    System.out.println("end game");
                    currentPlayer = 0;
                    winningPlayers[winnersAmount] = playerNames[currentPlayer];
                    break;
                }
                currentPlayer --;
            }

            //Check if card played has special effects and applies them.
            Card cardInPlay = discardPile.getTopCard();
            if (cardInPlay.getValue() == Value.DRAW_TWO)
            {
                hands[nextPlayer(rotation, currentPlayer, hands)].drawCards(drawPile, 2);
            } else if (cardInPlay.getValue() == Value.WILD_DRAW_FOUR)
            {
                hands[nextPlayer(rotation, currentPlayer, hands)].drawCards(drawPile, 4);
                System.out.println("Change color");
                String colorChange = sanitizeInput(scanner.nextLine());
                cardInPlay.setColor(Color.valueOf(colorChange));
            } else if (cardInPlay.getValue() == Value.SKIP)
            {
                currentPlayer = nextPlayer(rotation, currentPlayer, hands);
            } else if (cardInPlay.getValue() == Value.REVERSE)
            {
                if(rotation.equals("REVERSE"))
                {
                    rotation = "NORMAL";
                }
                else
                {
                    rotation = "REVERSE";
                }
            } else if (cardInPlay.getValue() == Value.WILD)
            {
                //create method for color change
                System.out.println("Change color");
                String colorChange = sanitizeInput(scanner.nextLine());
                cardInPlay.setColor(Color.valueOf(colorChange));
            }
            currentPlayer = nextPlayer(rotation, currentPlayer, hands);
        }
        return winningPlayers;
    }
    public static void printWinners(String[] winningPlayers)
    {
        System.out.println("Game Over!");
        System.out.println("Ranking:");
        for (int i = 0; i < winningPlayers.length; i++)
        {
            System.out.println("Rank " + i +":" + winningPlayers[i] + "!");
        }
    }
    public static int userInput(CardPile hand, CardPile discardPile)
    {
        String playInput;
        int numberInput = 0;

        while(true)
        {
            playInput = sanitizeInput(scanner.nextLine());
            if (hand.getLength() == 1)
            {
                if (playInput.equals("UNO"))
                {
                    System.out.println("Well Done!");
                    System.out.println("Play your last card");
                    playInput = sanitizeInput(scanner.nextLine());
                    break;
                }
                try
                {
                    numberInput = Integer.parseInt(playInput);
                }
                catch(Exception e)
                {
                    System.out.println("Invalid input! \n Please enter a valid number:");
                }
                if (numberInput == -1)
                {
                    return -1;
                }
                else if(numberInput<1 || numberInput > hand.getLength())
                {
                    System.out.println("You don't have this many cards! \n Please change input:");
                }
                else if (!hand.isPlaceable(discardPile,numberInput-1))
                {
                    System.out.println("This card is not playable! \n Please change input:");
                }
                else
                {
                    System.out.println("you forgot uno");
                    return -1;
                }
            }
            else
            {
                break;
            }
        }
        while(true)
        {
            try
            {
                numberInput = Integer.parseInt(playInput);
                if (numberInput == -1)
                {
                    return -1;
                }
                else if(numberInput<1 || numberInput > hand.getLength())
                {
                    System.out.println("You don't have this many cards! \n Please change input:");
                }
                else if (!hand.isPlaceable(discardPile,numberInput-1))
                {
                    System.out.println("This card is not playable! \n Please change input:");
                }
                else
                {
                    break;
                }
            } catch (Exception e)
            {
                System.out.println("Invalid input! \n Please enter a valid number:");
            }
            playInput = sanitizeInput(scanner.nextLine());
        }
        return numberInput;
    }
    public static String[] getPlayerNames(int playersAmount)
    {
        String[] playerNames = new String[playersAmount];
        //Loop to get each player's name
        for(int i=0;i< playersAmount;i++)
        {
            System.out.println("Player " + (i+1) + ":");
            String playerName = sanitizeInput(scanner.nextLine());
            boolean nameIsUnique = true;
            //Loop to keep asking for name until it is unique.
            while(true)
            {
                //Loop to compare name to previous names for uniqueness.
                for(int j = 0; j < i ; j++)
                {
                    if(playerName.equals(playerNames[j]))
                    {
                        System.out.println("Player name already exists!");
                        System.out.println("Please re-enter name:");
                        playerName = sanitizeInput(scanner.nextLine());
                        nameIsUnique = false;
                        break;
                    }
                }
                if(nameIsUnique)
                {
                    break;
                }
                nameIsUnique  = true;
            }
            playerNames[i] = playerName;
        }
        return playerNames;
    }

    public static int nextPlayer(String rotation, int currentPlayer, CardPile[] hands)
    {
        if(rotation.equals("NORMAL"))
        {
            if (currentPlayer == hands.length - 1)
            {
                currentPlayer = 0;
            } else
            {
                currentPlayer++;
            }
        }
        else if(rotation.equals("REVERSE"))
        {
            if (currentPlayer == 0)
            {
                currentPlayer = hands.length -1;
            } else
            {
                currentPlayer--;
            }
        }
        return currentPlayer;
    }
    public static String sanitizeInput(String input)
    {
        while(true)
        {
            if(input.charAt(0) == ' ')
            {
                input = input.substring(1);
            }
            else if(input.charAt(input.length()-1) == ' ')
            {
                input = input.substring(0,input.length()-1);
            }
            else
            {
                break;
            }
        }
        return input;
    }
    public static void flush()
    {
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }
    public static boolean canPLayLastCard()
    {
        String checkIfUno = sanitizeInput(scanner.nextLine());
        return (checkIfUno.equals("UNO"));
    }
    public static void sleep(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }


}