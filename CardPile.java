import java.util.*;
public class CardPile
{
    private Card[] cards;
    private int length;
    private Random randomNumGenerator;

    public CardPile()
    {
        this.cards = new Card[200];
        this.length = 0;
        randomNumGenerator = new Random();
    }
    public String toString()
    {
        //refractor
        String builder = "";
        for(int i = 0; i < this.length ; i += 6)
        {
            for(int j = 0;j < 7; j++)
            {
                for(int k = i; k < this.length && k < i+6; k++)
                {
                    builder += cards[k].getColor() + cards[k].asArray()[j];
                }
                builder += Color.RESET + "\n";
            }
            for(int j = i; j < this.length && j < i+6; j++)
            {
                builder += "      " + (j + 1) + "      ";
            }
            builder += Color.RESET + "\n";
        }
        return builder;
    }

    public void generateDrawPile()
    {
        //first check if discard pile is empty
        for(int i=0;i<4;i++)
        {
            this.cards[this.length] = new Card(Color.values()[i], Value.values()[0]);
            this.length++;
            this.cards[this.length] = new Card(Color.WILD, Value.values()[13]);
            this.length++;
            this.cards[this.length] = new Card(Color.WILD, Value.values()[14]);
            this.length++;
            for(int j=1;j<13;j++)
            {
                this.cards[this.length] = new Card(Color.values()[i],Value.values()[j]);
                this.length++;
                this.cards[this.length] = new Card(Color.values()[i],Value.values()[j]);
                this.length++;
            }
        }
//        for(int i=0;i<4;i++)
//        {
//            for(int j=1;j<2;j++)
//            {
//                this.cards[this.length] = new Card(Color.values()[i],Value.values()[j]);
//                this.length++;
//                this.cards[this.length] = new Card(Color.values()[i],Value.values()[j]);
//                this.length++;
//            }
//        }
    }


    private void addCard(Card card)
    {
        if(card.getValue() == Value.WILD)
        {
            card.setColor(Color.WILD);
        }
        this.cards[this.length] = card;
        length++;
    }
    public void drawCards(CardPile drawPile, int amountToDraw)
    {
        for(int i = 0; i < amountToDraw; i++)
        {
            if(drawPile.isEmpty())
            {
                drawPile.generateDrawPile();
            }
            int index = randomNumGenerator.nextInt(drawPile.length);
            addCard(drawPile.cards[index]);
            takeCard(drawPile, drawPile.cards[index]);
        }
    }
    public void place(CardPile discardPile, int cardToPlaceIndex)
    {
        Card cardInPlay = discardPile.cards[discardPile.length-1];
        if(isPlaceable(discardPile, cardToPlaceIndex))
        {
            discardPile.addCard(this.cards[cardToPlaceIndex]);
            cardInPlay.setColor(this.cards[cardToPlaceIndex].getColor());
            cardInPlay.setValue(this.cards[cardToPlaceIndex].getValue());
            takeCard(this, this.cards[cardToPlaceIndex]);

        }
        else
        {
            throw new IllegalArgumentException("ERROR: Card is not placeable!");
        }
    }
    public boolean isPlaceable(CardPile discardPile, int cardToPlaceIndex)
    {
        Card cardInPlay = discardPile.cards[discardPile.length-1];
        if(this.cards[cardToPlaceIndex].getColor().equals(cardInPlay.getColor()) || this.cards[cardToPlaceIndex].getValue().equals(cardInPlay.getValue()) || this.cards[cardToPlaceIndex].getColor().equals(Color.WILD))
        {
            return true;
        }
        return false;
    }

    private void takeCard(CardPile cardPile, Card card)
    {
        for(int i = 0; i < cardPile.length; i++)
        {
            if(cardPile.cards[i].equals(card))
            {
                for(int j = i; j < cardPile.length; j++)
                {
                    cardPile.cards[j] = cardPile.cards[j+1];
                }
                cardPile.length --;
                break;
            }
        }
    }


    public int getLength()
    {
        return this.length;
    }
    public Card getTopCard()
    {
        return this.cards[length-1];
    }
    public boolean isEmpty()
    {
        return (this.length == 0);
    }
}
