public enum Color
{
    RED,
    GREEN,
    BLUE,
    YELLOW,
    WILD,
    RESET;
    public String toString() {
        switch(this){
            case RESET:  return "\033[0m";
            case RED:    return "\033[31m";
            case GREEN:  return "\033[32m";
            case YELLOW: return "\033[33m";
            case BLUE:   return "\033[34m";
            case WILD:   return "\033[35m";
            default:     return "";
        }
    }
}

